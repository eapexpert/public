Feature: Cleanup Addresses in V3 Database Clients
Background: 
   Given User has existing Database (with ConnectionString)
   And User provided Database connection is accessible
   And User provides Google API Key
   And Google API can be reached and used
   And User is logged in to Address Cleanup utility
   When User attempts to run address cleanup utility
Scenario: Run Conversion Normally
   When address in EAP does not match what Google returns
   And number of records to process chosen is valid
   Then Address Cleanup presents the addresses returned from Google
   And user chooses the closest match addresses
   Then EAP address is updated along with storing the Google reference ID
  
Scenario: Database connection cannot be reached or accessed
   When User attempts to run address cleanup utility
   And Database is not accessible or able to be reached
   Then Address Cleanup utility notifies user connection could not be established and prompts them to verify their Database key and that it is assessible directly
 
Scenario: Database changes were not saved
   When User attempts to run address cleanup utility
   And changes to address(es) cannot be saved to the database
   Then Address Cleanup utility will not run and instead prompt user with error message and the ability to send the error log to us with additional details (like V3 for example)
  
Scenario: Record(s) already have Location ID set in Database
   When User runs address cleanup utility
   And records selected already have a Location ID
   Then Address Cleanup utility will skip these records
   And not do a Google lookup for said addresses

 Scenario: Multiple Google Map Addresses Available from one Address
   When User runs address cleanup utility
   And multiple Google Address are pulled for one Database address
   Then Address Cleanup utility will inform user of this and the results pulled so they can choose the most appropriate for the database

 Scenario: No results come from running address in cleanup utility
   When User runs address cleanup utility
   And no Google Addresses are available from that records search
   Then Address Cleanup utility will inform user of this and ask them to verify the information of that specific record (will make changes if user decides to update record)

 Scenario: Google API connection cannot be reached
   When User attempts to runs address cleanup utility
   And Google API connection cannot be reached
   Then Address Cleanup utility will inform user that the connection cannot be reached and will prompt to verify Google API key provided
