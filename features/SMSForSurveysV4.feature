Feature: Send Surveys via SMS
Background: 
   Given Beneficiary has a phone that is capable of SMS messages and phone calls
   And Beneficiary has provided mobile phone number to EAP Staff during First Intake
   And Twilio service is configured for use and can be reached
   And Beneficiary is opted into receiving SMS notifications
Scenario: Send appointment confirmation
   When Beneficiary receive an appointment reminder
   And Beneficiary is responding before 6h SMS timeout of occurs. Confirmation will go through if within 24hrs after being sent
   And Beneficiary confirms appointment with valid response of “C”
   Then Beneficiary receives a verification SMS of appointment being confirmed
 
Scenario: Twilio server cannot be reached from beneficiary`s mobile phone
   When Beneficiary receives an appointment reminder
   And Twilio server cannot be reached by beneficiary`s mobile phone
   Then EAP Company staff is informed in EAP Expert via an alert on login that the Twilio Server is down and a call will be done instead or email
 
Scenario: User is Opted-Out of SMS Notifications in EAP Expert (Intake Staff will take this info when getting mobile number on first intake)
   Given an appointment reminder needs to be sent
   When an EAP Company`s client has an appointment upcoming
   And Beneficiary is opted out of receiving SMS notifications
   Then EAP Company staff call Beneficiary to confirm or send email confirmation
 
Scenario: User appointment reminder timeout occurs
   Given an appointment reminder is sent
   And Beneficiary has not sent appointment confirmation within 6 hours
   Then EAP Company staff are informed in EAP Expert via an alert that a confirmation has not been received and call client directly to confirm
 
Scenario: Beneficiary sends appointment confirmation with wrong response
   When Beneficiary receives an appointment reminder
   And Beneficiary is responding before 6h SMS timeout of occurs. Confirmation will go through if within 24hrs after being sent
   And Beneficiary confirms appointment with an invalid response
   Then EAP Company staff are informed in EAP Expert via an alert that a confirmation has not received a correct response and call client directly to confirm
 
Scenario: Beneficiary sends SMS to number without initial prompt
   Given beneficiary has messaging client opened on my phone
   When Beneficiary send a SMS message to the appointment number without an initial prompt
    Then Beneficiary receives an automated SMS message stating there are no actions to be done
 
Scenario: User calls SMS number
   Given beneficiary has phone call client opened on my phone
   When Beneficiary call appointment reminder phone number
   Then Beneficiary receives an automated response stating no phone call functionality is available (can be customized but will require further research)
 
Scenario: Survey Questions Stop Midway (Twilio/Lack of Service)
   When Beneficiary stops receiving questions from survey phone number
   Then beneficiary`s received survey question responses are saved to the database and EAP Company staff are informed in EAP Expert for follow-up call

When Received Survey Responses are Inappropriate
   When Beneficiary survey responses are inappropriate and not feedback as requested
   Then survey is reviewed before being fully stored in EAP Expert for reporting